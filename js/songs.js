/*global angular  */

//'Injecting' the ngRoute module into my main App. This allows me to use the routing functionality which make my client more efficient and easier to manage
var myApp = angular.module('myApp', ['ngRoute']) // stating the main module with routing parameter


myApp.config( ['$routeProvider', $routeProvider => {  // here is the configuration for my single page App
  $routeProvider
    .when('/search', { 
      // if the a href or # matches 'search' the page will be controlled by the 'searchController' and the search template will be loaded 
		  // this section is the same for other controllers so I wont be commenting on them all
		  templateUrl: 'templates/search.html',
      controller: 'searchController'
		})
    .when('/detail/:id/:volumeInfo/:volumeInfoo/:instrument/:tuning', { 
      templateUrl: 'templates/detail.html',
      controller: 'detailController'
    })
    .when('/favourites', {
		  templateUrl: 'templates/favourites.html',
      controller: 'favouritesController'
		})
		.when('/login',{
		  templateUrl : 'templates/login.html',
		  controller: 'loginController'
		})
		.when('/register',{
		  templateUrl : 'templates/register.html',
		  controller: 'registerController'
		})
		.otherwise({
		  redirectTo: 'search' // if the route is invalid, the user will be redirected to the search route
		})
	}])

.run(function($rootScope) { // using rootscope to be able to share this object across other controllers
    $rootScope.test = 'Logged in as : ' // if no one is logged in display this
})
myApp.controller('searchController', ($scope, $http,$rootScope) => { // stating the controller used to deal with search route/template , the function includes $scope which allows to manipulate data and $http to perform GET requests
  $scope.message = 'Enter the name of the song You want to search'
  $scope.test = $rootScope.test
  $scope.search = $event => { // when the enter is pressed the function is triggered 
    console.log('search()')
    if ($event.which == 13) { // enter press , enter = key 13
      var search = $scope.searchTerm // this is the scope object which equals to the users input
      console.log(search)
      var url = 'https://api-mchael.c9users.io/songs?query='+search // using my API to search for the desired song
      $http.get(url).success(response => { // getting the response from the url listed above 
        console.log(response)
        $scope.songs = response.data // $scope.songs is my main scope object which is being maipulated in other controllers, the response API got is the data , which is displayed when $scope.songs is called
        $scope.searchTerm = ''
      })
    }
  }
})

myApp.controller('detailController', ($scope, $routeParams,$rootScope) => { // detail controller uses standart $scope and $routeParams to get the data ,which was returned in the search controller and display it
  console.log("Entering detail controller")
  $scope.test = $rootScope.test
  $scope.message = 'This is the detail screen'
  $scope.ID = $routeParams.id // setting the scope to equal the data flagged as {{song.ID}} in the search template and as ':id' in the route settings
  $scope.Title = $routeParams.volumeInfo // setting the scope to equal the data flagged as {{song.Title}} in the search template and as ':volumeInfo' in the route settings
  $scope.Authors = $routeParams.volumeInfoo // setting the scope to equal the data flagged as {{song.Authors}} in the search template and as ':volumeInfoo' in the route settings
  $scope.Instrument = $routeParams.instrument // setting the scope to equal the data flagged as {{song.Instrument}} in the search template and as ':instrument' in the route settings
  $scope.Tuning = $routeParams.tuning  // setting the scope to equal the data flagged as {{song.Tuning}} in the search template and as ':tuning' in the route settings
  $scope.addToFavourites = stuff => { //creating a new scope object to store the data to favourites 
    var stuff = {
    id : $routeParams.id,
    title : $routeParams.volumeInfo+"  by :  "+$routeParams.volumeInfoo,
    authors: $routeParams.volumeInfoo
    //link: $routeParams.selfLink
    }
    console.log('adding: '+JSON.stringify(stuff)+' to favourites.')
    localStorage.setItem(stuff.title,stuff.title) // storing the favourites in the localstorage of the web browser
    console.log(localStorage.getItem(stuff)) // returning the stored item
  }
  
})


myApp.controller('loginController', ($scope,$rootScope) => {
  $scope.message = 'This is the Login screen'
  $scope.login = () => { // using this function and linking it with a button to validate the entered username and password
				var username = $scope.username
    		var password = $scope.password
				/* create a base64 encoded string */
				var encoded = window.btoa(username+':'+password) // encoding the account to see if it matches with the registered ones
				var encoded1 = window.btoa(username)
				console.log(encoded)
				var decoded = window.atob(encoded1)
				for (var i in localStorage){ // looping throught localStorage to check if accounts match
				  if(i == encoded){ // if matches
				    $scope.logged = 'Welcome' // display 'Welcome'
				    window.alert("Welcome")
				    $rootScope.test = 'Logged in as : '+decoded // if login successful , username will be displayed as logged in
				    window.location = "#/search"; // automatic redirect to the search route
				  }else {
				    $scope.message4 = "Try Again"
				  
				  }
				}
			},
			
			$scope.clear = () =>{ // basic function to clear the text input if the button linked to this function is pressed
			  var clear = window.location.href='#login' // clearing is done by loading the template again
    
			}
 
  
})
myApp.controller('registerController', ($scope,$rootScope) => { 
  $scope.message = 'This is the Register'
  $scope.test = $rootScope.test
   $scope.register = () => {  // uses the same code as the login controller but with validation
				var username = $scope.user
				
    		var password = $scope.pass
    		
    		console.log("valid scope "+$scope.form.$valid)
    		console.log("username length "+$scope.user.length)
    		console.log("pass length "+$scope.pass.length)
				/* create a base64 encoded string */
				var encoded = window.btoa(username+':'+password)
				console.log(encoded)
				if ($scope.form.$valid==false && $scope.user.length==undefined && $scope.pass.length==undefined){ // checking if the form where the username and password are written pass the validation
				// if the register template validation failed or the length of entered username/password is undefined
				  window.alert("FILL IN USERNAME/PASSWORD") // alert the user
				}else if($scope.user.length && $scope.pass.length < 5){ // if length of username/password is less than 5 
				  window.alert("user/password have to be longer than 5 characters") // alert the user
				
				}else if($scope.form.$valid==true){ // if validation is true
				var dummy = 'dummy'
				var dummy1 = 'dummy1'
				var A = window.btoa(dummy+dummy1)
				localStorage.setItem(A,A) // adding a dummy to the localStorage , without it the account doesnt want to be stored. The reason is unknown
  			for (var i in localStorage){ // check if username already taken
  			  if (i == encoded){
  			    window.alert('Username Already Taken')
  			    window.location.reload()
  			  }else  { // if not add them to the localStorage
  			    localStorage.setItem(encoded,encoded)
  			    console.log("stored in storage")
  			    $scope.message1 = 'Account Created'
  			    
  			  }
  			}
				}
     },
   
				
			
		
			$scope.clear = () =>{
			  var clear = window.location.href='#register'
			}
   
  
})
 
myApp.controller('favouritesController', ($scope,$rootScope) => {
  console.log('fav controller')
  $scope.message = 'This is the favourites screen'
  $scope.test = $rootScope.test
 
  var init = () => {
    console.log('getting books')
    var items = Array() // creating array to display the favourites 
    console.log(localStorage)
    for (var a in localStorage) { // looping through the localStorage to get data back and display it all
      console.log("a:"+a)
      
      items.push(localStorage[a]) // adding the data found in LocalStorage to the array
      
    }
    //console.log(items.id)
    //console.log($scope.books)
    $scope.songs = items
    console.log($scope.songs)
    //$scope.books.id = items.id
    //$scope.books.title = items.title
  }
  init()
   $scope.delete = (song) => { // function to delete selected favourite from the localStorage
    localStorage.removeItem(song)
    window.location.href='#detail' //simple redirection forward and back to clear the deleted data from the page
    window.location.href='#favourites'
  }
})
